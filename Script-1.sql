CREATE TABLE test(

      id serial primary key,

      name varchar,
      
      age int8,
      
      settings jsonb
      

);
INSERT INTO public.test
("name", age, settings)
VALUES('test', 12, '{"email":"asd@mailru"}');

UPDATE public.test
SET "name"='test123123', age=345345, settings='{"email":"as123123d@mailru"}'

DELETE FROM public.test

