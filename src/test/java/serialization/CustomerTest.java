package serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    @Test
    public void confertToJSON() {
        final Gson gson = new GsonBuilder().create();
        FullName fullName = new FullName("test", "Testovich","Testov");
        Customer customer = new Customer(true,1L,"MSK", fullName, new String[]{"test@mail.ru"});
        assertEquals(customer.toString(),gson.toJson(customer));
    }


}