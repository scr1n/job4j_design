package srp.reports;

import org.junit.Test;

import java.util.Calendar;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DeveloperReportEngineTest {
    @Test
    public void whenOldGenerated() {
        MemStore store = new MemStore();
        Calendar now = Calendar.getInstance();
        Employee worker = new Employee("Ivan", now, now, 100);
        store.add(worker);
        Report engine = new DeveloperReportEngine(store);
        StringBuilder expect = new StringBuilder()
        .append("<html>")
        .append("<h4>")
                .append("Name; Hired; Fired; Salary")
                .append("</h4>")
                .append(worker.getName()).append(";")
                .append(worker.getHired()).append(";")
                .append(worker.getFired()).append(";")
                .append(worker.getSalary()).append(";")
                .append(System.lineSeparator())
        .append("</html>");
        assertThat(engine.generate(em -> true), is(expect.toString()));
    }
}