package list;

import org.junit.Test;

import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ListUtilsTest {
    @Test
    public void whenAddBefore() {
        List<Integer> input = new ArrayList<>(Arrays.asList(1, 3));
        ListUtils.addBefore(input, 1, 2);

        assertThat(input, is(Arrays.asList(1, 2, 3)));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void whenAddBeforeWithInvalidIndex() {
        List<Integer> input = new ArrayList<>(Arrays.asList(1, 3));
        ListUtils.addBefore(input, 3, 2);
    }

    @Test
    public void whenAddAfterLast() {
        List<Integer> input = new ArrayList<>(Arrays.asList(0, 1, 2));
        ListUtils.addAfter(input, 2, 3);

        assertThat(input, is(Arrays.asList(0, 1, 2, 3)));
    }

    @Test
    public void whenRemoveIf() {
        List<Integer> input = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
        ListUtils.removeIf(input, integer -> integer.equals(2));
        assertThat(input, is(Arrays.asList(0, 1, 3)));
    }

    @Test
    public void whenMultiRemoveIf() {
        List<Integer> input = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
        ListUtils.removeIf(input, integer -> integer.equals(2) || integer.equals(3));
        assertThat(input, is(Arrays.asList(0, 1)));
    }

    @Test
    public void whenReplaceIf() {
        List<Integer> input = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
        ListUtils.replaceIf(input, integer -> integer.equals(1), 2);
        assertThat(input, is(Arrays.asList(0, 2, 2, 3)));
    }

    @Test
    public void whenMultiReplaceIf() {
        List<Integer> input = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
        ListUtils.replaceIf(input, integer -> integer.equals(1) || integer.equals(3), 2);
        assertThat(input, is(Arrays.asList(0, 2, 2, 2)));
    }

    @Test
    public void whenRemoveAll() {
        List<Integer> input = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
        List<Integer> list = new ArrayList<>(Arrays.asList(0, 1));
        ListUtils.removeAll(input, list);
        assertThat(input, is(Arrays.asList(2, 3)));
    }

    @Test
    public void whenRemoveAll2() {
        List<Integer> input = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
        List<Integer> list = new ArrayList<>(Arrays.asList(0, 5));
        ListUtils.removeAll(input, list);
        assertThat(input, is(Arrays.asList(1, 2, 3)));
    }
}