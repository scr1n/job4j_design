package list.map;

import org.junit.Test;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class SimpleMapTest {
    @Test
    public void whenAddInMap() {
        SimpleMap<Integer, String> map = new SimpleMap<>();
        map.put(1, "Nikita");
        assertThat(map.get(1), is("Nikita"));
    }

    @Test
    public void whenDeleteInMap() {
        SimpleMap<Integer, String> map = new SimpleMap<>();
        map.put(1, "Nikita");
        map.put(2, "Mira");
        assertTrue(map.remove(1));
        assertNotEquals(map.get(1), "Nikita");
    }

    @Test
    public void whenDeleteNotExistElementInMap() {
        SimpleMap<Integer, String> map = new SimpleMap<>();
        map.put(1, "Nikita");
        map.put(2, "Mira");
        assertFalse(map.remove(3));
    }

    @Test(expected = ConcurrentModificationException.class)
    public void whenCrashIterator() {
        SimpleMap<Integer, String> map = new SimpleMap<>();
        map.put(1, "Nikita");
        map.put(2, "Mira");
        Iterator<Integer> iterator = map.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            map.put(3, "Dmitriy");
        }
    }

    @Test(expected = NoSuchElementException.class)
    public void whenIfMapIsEmptyCrashIterator() {
        SimpleMap<Integer, String> map = new SimpleMap<>();
        Iterator<Integer> iterator = map.iterator();
        iterator.next();
    }

    @Test
    public void expandMap() {
        SimpleMap<Integer, String> map = new SimpleMap<>(1);
        map.put(1, "Nikita");
        assertTrue(map.put(467, "Andrew"));
    }
}