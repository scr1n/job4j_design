package list.map;

import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.hash;
import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void create() {
        Calendar calendar = new GregorianCalendar(2021, Calendar.OCTOBER, 1);
        User one = new User("test", 1, calendar);
        User two = new User("test", 1, calendar);
        System.out.println(hash(one.hashCode()));
        System.out.println(hash(two.hashCode()));
        Map map = new HashMap();
        map.put(one, new Object());
        map.put(two, new Object());
        System.out.println(map);
        System.out.println(map.size());
    }

}