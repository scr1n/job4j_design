package io;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class ConfigTest {

    @Test
    public void whenPairWithoutComment() {
        String path = "app.properties";
        Config config = new Config(path);
        config.load();
        assertThat(config.value("name"), is("test"));
    }

    @Test
    public void whenKeyWithoutValue() {
        String path = "app.properties";
        Config config = new Config(path);
        config.load();
        assertThat(config.value("mainname"), is(Matchers.nullValue()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenIllegalArgument() {
        String path = "_app.properties";
        Config config = new Config(path);
        config.load();
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenIllegalValue() {
        String path = "_app.properties";
        Config config = new Config(path);
        config.load();
    }

}