package list.linked;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ForwardLinked<T> implements Iterable<T> {
    private Node<T> head;
    public int size = 0;

    public T deleteFirst() {
        Node<T> prev = head;
        if (prev == null) {
            throw new NoSuchElementException();
        }
        final T value1 = prev.value;
        head = head.next;
        prev.next = null;
        size--;
        return value1;
    }

    public boolean revert() {
        Node reversedPart = null;
        Node current = head;

        if (isEmpty() || current.next == null) {
            return false;
        }

        while (current != null) {
            Node next = current.next;
            current.next = reversedPart;
            reversedPart = current;
            current = next;
        }
        head = reversedPart;
        return true;
    }

    public void addFirst(T first) {
        head = new Node<T>(first, head);
        size++;
    }

    public void add(T value) {
        Node<T> node = new Node<T>(value, null);
        if (head == null) {
            head = node;
            return;
        }
        Node<T> tail = head;
        while (tail.next != null) {
            tail = tail.next;
        }
        tail.next = node;
        size++;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> node = head;

            @Override
            public boolean hasNext() {
                return node != null;
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                T value = node.value;
                node = node.next;
                return value;
            }
        };
    }

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }
    }
}
