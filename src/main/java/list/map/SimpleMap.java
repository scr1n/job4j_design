package list.map;

import java.util.*;

public class SimpleMap<K, V> implements Map<K, V> {

    private static final float LOAD_FACTOR = 0.75f;

    private int capacity = 4;

    private int count = 0;

    private int modCount = 0;

    private MapEntry<K, V>[] table;

    public SimpleMap() {
        table = new MapEntry[capacity];
    }

    public SimpleMap(int capacity) {
        this.capacity = capacity;
        table = new MapEntry[capacity];
    }

    @Override
    public boolean put(K key, V value) {
        int hash = key == null ? 0 : hash(key.hashCode());
        if (count == table.length) {
            expand();
        }
        new ArrayList<>().add(null);
        new HashSet<>().add(null);
        int arrayIndex = indexFor(hash);
        if (table[arrayIndex] == null) {
            table[arrayIndex] = new MapEntry<>(key, value);
            count++;
            modCount++;
            return true;
        }
        return false;
    }

    private int hash(int hashCode) {
        return hashCode ^ (hashCode >>> 16);
    }

    private int indexFor(int hash) {
        return (table.length - 1) & hash;
    }

    private void expand() {
        if (count >= table.length * LOAD_FACTOR) {
            MapEntry<K, V>[] newTable = new MapEntry[table.length * 2];
            for (int i = 0; i < table.length; i++) {
                if (table[i] != null && !Objects.equals(table[i].key, null)) {
                    int hash = table[i].key == null ? 0 : hash(table[i].key.hashCode());
                    newTable[indexFor(hash)] = table[i];
                }
            }
            table = newTable;
        }
    }

    @Override
    public V get(K key) {
        int arrayIndex = indexFor(hash(key.hashCode()));
        if (table[arrayIndex] == null) {
            return null;
        }
        return table[arrayIndex].value;

    }

    @Override
    public boolean remove(K key) {
        boolean rsl = false;
        int arrayIndex = indexFor(hash(key.hashCode()));
        MapEntry<K, V> node = table[arrayIndex];
        if (node != null) {
            if ((key == null && node.key == null) || (key != null && key.equals(node.key))) {
                table[arrayIndex] = null;
                rsl = true;
                count--;
                modCount++;
            }
        }
        return rsl;
    }

    @Override
    public Iterator<K> iterator() {
        return new Iterator<>() {
            private final int expectedModCount = modCount;
            private int index = 0;

            @Override
            public boolean hasNext() {
                boolean rsl = false;
                for (int i = index; i < table.length; i++) {
                    if (table[i] != null) {
                        index = i;
                        rsl = true;
                        break;
                    }
                }
                return rsl;
            }

            @Override
            public K next() throws NoSuchElementException, ConcurrentModificationException {
                if (modCount != expectedModCount) {
                    throw new ConcurrentModificationException();
                }
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return table[index++].key;
            }
        };
    }


    private static class MapEntry<K, V> {

        K key;
        V value;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

    }

}
