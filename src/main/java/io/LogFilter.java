package io;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LogFilter {
    public static List<String> filter(String filePath) {
        ArrayList<String> filteredLines = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(filePath))) {
            Object[] lines = in.lines().toArray();
            for (Object line : lines) {
                String[] splitArrray = ((String) line).split(" ");
                String neededLine = splitArrray[splitArrray.length - 2];
                if (neededLine.equals("404")) {
                    filteredLines.add(line + System.lineSeparator());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filteredLines;
    }

    public static void save(List<String> log, String file) {
        try (PrintWriter out = new PrintWriter(
                new BufferedOutputStream(
                        new FileOutputStream(file)
                ))) {

            for (String line : log) {
                out.printf("%s%n", line);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        List<String> log = filter("log.txt");
        save(log, "404.txt");
    }
}
