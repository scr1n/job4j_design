package io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleChat {

    private final String path;
    private final String botAnswers;
    private final List<String> logs = new ArrayList<>();
    public List<String> phrases;
    private static final String OUT = "закончить";
    private static final String STOP = "стоп";
    private static final String CONTINUE = "продолжить";

    public ConsoleChat(String path, String botAnswers) {
        this.path = path;
        this.botAnswers = botAnswers;
        phrases = readPhrases();
    }

    public void run() {
        boolean botIsActive = true;
        Scanner scanner = new Scanner(System.in);

        String botAnswer;
        System.out.println("Давай  базарь,пес");
        while (true) {
            String question = scanner.nextLine();
            switch (question) {
                case OUT -> {
                    logs.add(OUT);
                    saveLog(logs);
                    scanner.close();
                    return;
                }
                case STOP -> {
                    botIsActive = false;
                    logs.add(question);
                }
                case CONTINUE -> {
                    logs.add(CONTINUE);
                    botIsActive = true;
                    botAnswer = getBotAnswers();
                    System.out.println(botAnswer);
                    logs.add(botAnswer);
                }
                default -> {
                    logs.add(question);
                    if (botIsActive) {
                        botAnswer = getBotAnswers();
                        System.out.println(botAnswer);
                        logs.add(botAnswer);
                    }
                }
            }
        }

    }

    private List<String> readPhrases() {
        List<String> phrases = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(botAnswers))) {
            br.lines().forEach(phrases::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return phrases;
    }

    private void saveLog(List<String> log) {
        try (PrintWriter bwr = new PrintWriter(new FileWriter(path, StandardCharsets.UTF_8, true))) {
            log.forEach(bwr::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String getBotAnswers() {
        return phrases.get((int) (Math.random() * phrases.size()));
    }

    public static void main(String[] args) {
        ConsoleChat cc = new ConsoleChat("conversations.txt", "answers.txt");
        cc.run();
    }
}
