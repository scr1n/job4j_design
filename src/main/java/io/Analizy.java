package io;

import java.io.*;
import java.util.Objects;

public class Analizy {
    public void unavailable(String source, String target) {
        try (BufferedReader br = new BufferedReader(new FileReader(source));
             BufferedWriter bw = new BufferedWriter(new FileWriter(target))) {
            StringBuilder temp = new StringBuilder();
            String[] prev = {"0", "0"};
            String next = br.readLine();
            while (next != null) {
                String[] arr = next.split(" ");
                if ((Objects.equals(arr[0], "400") || Objects.equals(arr[0], "500"))
                        && !(Objects.equals(prev[0], "400") || Objects.equals(prev[0], "500"))) {
                    temp.append(arr[1]).append(";");
                }
                if (!(Objects.equals(arr[0], "400") || Objects.equals(arr[0], "500"))
                        && (Objects.equals(prev[0], "400") || Objects.equals(prev[0], "500"))) {
                    temp.append(arr[1]).append(";").append(System.lineSeparator());
                }
                prev = arr;
                next = br.readLine();
            }
            bw.write(temp.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Analizy sample = new Analizy();
        sample.unavailable("unavailable.csv", "target.log");
    }
}
