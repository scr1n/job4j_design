package io;

import java.io.File;

public class Dir {
    public static void main(String[] args) {
        File file = new File("F:\\projects\\job4j_design");
        if (!file.exists()) {
            throw new IllegalArgumentException(String.format("Not exist %s", file.getAbsoluteFile()));
        }
        if (!file.isDirectory()) {
            throw new IllegalArgumentException(String.format("Not directory %s", file.getAbsoluteFile()));
        }
        for (File subfile : file.listFiles()) {
            System.out.print("File name: " + subfile.getName());
            System.out.print("File size: " + subfile.length());
            System.out.println();
        }
    }
}
