package io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;

public class Search {
    public static void main(String[] args) throws IOException {
        validate(args);
        Path start = Paths.get(args[0]);
        search(start, p -> p.toFile().getName().endsWith(args[1])).forEach(System.out::println);
    }

    private static void validate(String[] args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("Runing parametrs are empty");
        }
        File rootPath = Paths.get(args[0]).toFile();
        if (args[0].isEmpty() || args[1].isEmpty()) {
            throw new IllegalArgumentException("Root path or Extension  is empty");
        }
        if (!rootPath.exists()) {
            throw new IllegalArgumentException(String.format("Not exist %s", rootPath.getAbsoluteFile()));
        }

        if (!rootPath.isDirectory()) {
            throw new IllegalArgumentException(String.format("Not directory %s", rootPath.getAbsoluteFile()));
        }

    }

    public static List<Path> search(Path root, Predicate<Path> condition) throws IOException {
        SearchFiles searcher = new SearchFiles(condition);
        Files.walkFileTree(root, searcher);
        return searcher.getPaths();
    }
}
