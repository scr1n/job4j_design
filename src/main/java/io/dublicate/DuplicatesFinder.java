package io.dublicate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class DuplicatesFinder {
    public static void main(String[] args) throws IOException {
        DuplicatesVisitor visitor = new DuplicatesVisitor();
        Files.walkFileTree(Path.of("./"), visitor);
        List<Path> list = visitor.getPaths();
        if (list.isEmpty()) {
            System.out.println("Дубликаты не найдены");
        } else {
            System.out.println("Найдены следующие дубликаты:");
            list.forEach(System.out::println);
        }
    }
}
