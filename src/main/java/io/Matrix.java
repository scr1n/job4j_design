package io;

import io.calculator.Calculator;

import java.io.FileOutputStream;
import java.io.IOException;

public class Matrix {
    public static int[][] multiple(int count) {
        int[][] multi = new int[count][count];
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < count; j++) {
                multi[i][j] = (i + 1) * (j + 1);
            }
        }
        multiToFile(multi);
        return multi;
    }

    private static String multi(int[][] data) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length; j++) {
                builder.append((i + 1) * (j + 1)).append(" ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    public static void multiToFile(int[][] data) {
        String multi = multi(data);
        try (FileOutputStream outputStream = new FileOutputStream("resultCalculator1.txt")) {
            outputStream.write(multi.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
