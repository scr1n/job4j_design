package io.calculator;

import java.io.FileOutputStream;
import java.io.IOException;

public class CalculatorFile {
    public static void main(String[] args) {
        multiToFile();
    }

    public static void multiToFile() {
        String multi = Calculator.multi(4);
        try (FileOutputStream outputStream = new FileOutputStream("resultCalculator.txt")) {
            outputStream.write(multi.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
