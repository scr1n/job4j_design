package io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zip {
    public static void archiving(String path, Predicate<Path> predicate, String nameZip) throws IOException {
        List<Path> all = Search.search(Path.of(path), predicate);
        List<File> save = all.stream().map(Path::toFile).collect(Collectors.toList());
        packFiles(save, new File(nameZip));
    }

    public static void packFiles(List<File> sources, File target) {
        try (ZipOutputStream zip = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(target)))) {
            for (File file : sources) {
                zip.putNextEntry(new ZipEntry(file.getPath()));
                try (BufferedInputStream out = new BufferedInputStream(new FileInputStream(file))) {
                    zip.write(out.readAllBytes());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        ArgsName argsName = ArgsName.of(args);
        validate(argsName.get("d"), p -> !p.toString().endsWith(argsName.get("e")), argsName.get("o"));
        archiving(argsName.get("d"), p -> !p.toString().endsWith(argsName.get("e")), argsName.get("o"));
    }

    static void validate(String path, Predicate<Path> predicate, String nameZip) {
        if (path == null || predicate == null || nameZip == null) {
            throw new IllegalArgumentException("Parametrs are not be null");
        }
        Path pas = Path.of(path);
        if (!Files.exists(pas)) {
            throw new IllegalArgumentException(String.format("Not exist %s", Path.of(path)));
        }
    }
}
