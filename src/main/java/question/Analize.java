package question;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Analize {
    public static Info diff(Set<User> previous, Set<User> current) {
        Map<Integer, User> currentMap = current.stream().collect(Collectors.toMap(User::getId, user -> user));
        int deleted = 0;
        int changed = 0;
        for (User previousUser : previous) {
            if (!currentMap.containsKey(previousUser.getId())) {
                deleted++;
            }
            User currentUser = currentMap.remove(previousUser.getId());
            if (currentUser != null && !previousUser.getName().equals(currentUser.getName())) {
                changed++;
            }
        }
        return new Info(currentMap.size(), changed, deleted);
    }
}
