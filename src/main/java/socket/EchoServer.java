package socket;

import logs.UsageLog4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
    private static final Logger LOG = LoggerFactory.getLogger(UsageLog4j.class.getName());

    public static void main(String[] args) {
        try (ServerSocket server = new ServerSocket(9000)) {
            while (!server.isClosed()) {
                Socket socket = server.accept();
                try (OutputStream out = socket.getOutputStream();
                     BufferedReader in = new BufferedReader(
                             new InputStreamReader(socket.getInputStream()))) {
                    String dataFromClient = in.readLine();
                    String answerText = "Hello, dear friend.";
                    if (!dataFromClient.isEmpty()) {
                        if (dataFromClient.contains("?msg=Exit")) {
                            answerText = "Bye!\r\n";
                            server.close();
                        } else if (dataFromClient.contains("?msg=Hello")) {
                            answerText = "Hello!\r\n";
                        } else if (dataFromClient.contains("?msg=What")) {
                            answerText = "What?\r\n";
                        }
                        out.write("HTTP/1.1 200 OK\r\n\r\n".getBytes());
                        out.write(answerText.getBytes());
                        System.out.println(answerText);
                        out.flush();
                    }


                }
            }
        } catch (Exception e) {
            LOG.error("Произошла ошибка при работе бота", e);
        }
    }
}
