package srp.reports;

import java.util.function.Predicate;

public class AccountReportEngine implements Report {
    private final Store store;

    public AccountReportEngine(Store store) {
        this.store = store;
    }

    @Override
    public String generate(Predicate<Employee> filter) {
        StringBuilder text = new StringBuilder();
        text.append("Name; Hired; Fired; Salary");
        for (Employee employee : store.findBy(filter)) {
            text.append(employee.getName()).append(";")
                    .append(employee.getHired()).append(";")
                    .append(employee.getFired()).append(";")
                    .append(employee.getSalary() / 75).append("$").append(";")
                    .append(System.lineSeparator());
        }
        return text.toString();
    }
}
