package logs;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UsageLog4j {
    private static final Logger LOG = LoggerFactory.getLogger(UsageLog4j.class.getName());

    public static void main(String[] args) {
        BasicConfigurator.configure();
        String name = "Petr Arsentev";
        int age = 33;
        byte isMered = 1;
        char sex = 'M';
        double salary = 123.1d;
        long molekula = 1_000_000L;
        boolean isAlive = true;
        short count = 12;
        float countCheese = 13.1f;

        LOG.debug("User info name : {}, age : {}", name, age);
        LOG.info("User info mered : {}, sex : {}", isMered, sex);
        LOG.warn("User info salary : {}, molekula : {}", salary, molekula);
        LOG.error("User info isAlive : {}, count : {}, countCheese: {}", isAlive, count, countCheese);
    }
}
