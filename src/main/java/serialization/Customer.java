package serialization;

import javax.xml.bind.annotation.*;
import java.util.Arrays;

@XmlRootElement(name = "customer")
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer {
    @XmlAttribute
    boolean isActive;
    @XmlAttribute
    long id;
    @XmlAttribute
    String city;
    FullName fullName;
    @XmlElementWrapper(name = "emails")
    @XmlElement(name = "email")
    String[] emails;

    public Customer() {
    }

    public Customer(boolean isActive, long id, String city, FullName fullName, String[] emails) {
        this.isActive = isActive;
        this.id = id;
        this.city = city;
        this.fullName = fullName;
        this.emails = emails;
    }

    public boolean isActive() {
        return isActive;
    }

    public long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public FullName getFullName() {
        return fullName;
    }

    public String[] getEmails() {
        return emails;
    }

    @Override
    public String toString() {
        return "Customer{"
                + "isActive=" + isActive
                + ", id=" + id
                + ", name='" + city + '\''
                + ", fullName=" + fullName
                + ", emails=" + Arrays.toString(emails)
                + '}';
    }

    public void setFullName(FullName fullName) {
        this.fullName = fullName;
    }

}
