package serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONObject;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;

public class CustomerConverter {
    /**
     *  Формат JSON [#313164 #213966]
     public static void main(String[] args) throws IOException, ClassNotFoundException {
        final Gson gson = new GsonBuilder().create();
        FullName fullName = new FullName("test", "Testovich", "Testov");
        Customer customer = new Customer(true, 1L, "MSK", fullName, new String[]{"test@mail.ru"});
        String customerJson = gson.toJson(customer);
        Customer asd = gson.fromJson(customerJson, Customer.class);
    }
     */

    /**
     *
     * JAXB. Преобразование XML в POJO. [#315063 #213974]
    public static void main(String[] args) throws IOException, ClassNotFoundException, JAXBException {
        FullName fullName = new FullName("test", "Testovich", "Testov");
        Customer customer = new Customer(true, 1L, "MSK", fullName, new String[]{"test@mail.ru"});
        JAXBContext context = JAXBContext.newInstance(Customer.class);

        Marshaller marshaller = context.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        String xml = "";
        try (StringWriter writer = new StringWriter()) {
            // Сериализуем
            marshaller.marshal(customer, writer);
            xml = writer.getBuffer().toString();
            System.out.println(xml);
        }

        Unmarshaller unmarshaller = context.createUnmarshaller();
        try (StringReader reader = new StringReader(xml)) {

            Customer result = (Customer) unmarshaller.unmarshal(reader);
            System.out.println(result);
        }
    }
     */

    /**
     * Зацикливание
     * @param args
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FullName fullName = new FullName("test", "Testovich", "Testov");
        Customer customer = new Customer(true, 1L, "MSK", fullName, new String[]{"test@mail.ru"});
        fullName.setCustomer(customer);
        customer.setFullName(fullName);
        System.out.println(new JSONObject(customer));
    }
}
