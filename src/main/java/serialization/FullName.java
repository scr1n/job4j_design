package serialization;

import org.json.JSONPropertyIgnore;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fullName")
public class FullName {
    private Customer customer;
    @XmlAttribute
    String name;
    @XmlAttribute
    String firstName;
    @XmlAttribute
    String lastName;

    public FullName() {
    }

    public FullName(String name, String firstName, String lastName) {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public FullName(String name, String firstName, String lastName, Customer customer) {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.customer = customer;
    }

    public String getName() {
        return name;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    @JSONPropertyIgnore
    public Customer getCustomer() {
        return customer;
    }

    @Override
    public String toString() {
        return "FullName{"
                + "name='" + name + '\''
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + '}';
    }
}
